package ro.orange;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        // declarare variabile
        String[] numereceptionist = new String[5];
        int[] etaj = new int[5];
        int i;
        String [][] multi = new String[2][2];
        String [] hotel = new String[1];
        char [] security = {'B','E','D','O','N','A','L','D','A','T','E'};
        char [] sec2 = new char[6];

        // initializare variabile
        numereceptionist[0] = "John Snow";
        numereceptionist[1] = "William Cooper";
        numereceptionist[2] = "Patricia Johnson";
        numereceptionist[3] = "Kate Anderson";
        numereceptionist[4] = "Dustin Rhodes";

        multi[0][0] = "Mr.";
        multi[0][1] = " Mrs.";
        multi[1][0] = " Smith Riley";
        multi[1][1] = " Catherine Jade";

        hotel[0] = "Plaza Hotel";

        for (i = 0; i < 5; i++) {
            etaj[i] = i+1;
            System.out.println("The receptionist on " + etaj[i] + " floor is: " + numereceptionist[i]);
        }

        System.out.println(multi[0][0] + multi[1][0] + " and" + multi[0][1] + multi[1][1] + " are the owners of " +
                hotel[0]);

        System.arraycopy(security,2, sec2, 0, 6);
        String altnume = new String(sec2);     // pentru a elimina virgulele

        System.out.println("The hotel's security is provided by: "+ altnume);


    }
}

